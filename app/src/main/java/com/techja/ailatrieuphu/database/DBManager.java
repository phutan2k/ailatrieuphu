package com.techja.ailatrieuphu.database;

import com.techja.ailatrieuphu.App;
import com.techja.ailatrieuphu.MTask;
import com.techja.ailatrieuphu.database.entities.HighScore;
import com.techja.ailatrieuphu.database.entities.Question;

import java.util.ArrayList;
import java.util.List;

public class DBManager {
    private static DBManager instance;

    public DBManager() {
        // for singleton
    }

    public static DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    public void getAllQuestionInOneTurn(OnResultCallBack cb) {
        new Thread() {
            @Override
            public void run() {
                try {
                    List<Question> listQuestion = App.getInstance().getQuestionDB().
                            getQuestionDAO().getAllQuestionInOneTurn();
                    cb.callBack(listQuestion);
                } catch (Exception e) {
                    cb.callBack(new ArrayList<Question>());
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void getQuestionByAsyncTask(int level, OnResultCallBack cb) {
        MTask mTask = new MTask(null, new MTask.MTaskListener() {
            @Override
            public Object execTask(Object dataInput, String key, MTask task) {
                return App.getInstance().getQuestionDB().getQuestionDAO().getQuestionForLevel(level);
            }

            @Override
            public void completeTask(Object result, String key) {
                cb.callBack(result);
            }
        });
        mTask.startAsync(null);
    }

    public void getQuestionForLevel(int level, OnResultCallBack cb) {
        new Thread() {
            @Override
            public void run() {
                try {
                    Question question = App.getInstance().getQuestionDB().
                            getQuestionDAO().getQuestionForLevel(level);
                    cb.callBack(question);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void addScore(HighScore highScore, OnResultCallBack cb) {
        MTask mTask = new MTask(null, new MTask.MTaskListener() {
            @Override
            public Object execTask(Object dataInput, String key, MTask task) {
                try {
                    App.getInstance().getQuestionDB().getQuestionDAO().addScore((HighScore) dataInput);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }

            @Override
            public void completeTask(Object result, String key) {
                cb.callBack(result);
            }
        });

        mTask.startAsyncTask(highScore);
    }

    public void getListHighScore(OnResultCallBack cb) {
        MTask mTask = new MTask(null, new MTask.MTaskListener() {
            @Override
            public Object execTask(Object dataInput, String key, MTask task) {
                return App.getInstance().getQuestionDB().getQuestionDAO().getListHighScore();
            }

            @Override
            public void completeTask(Object result, String key) {
                cb.callBack(result);
            }
        });

        mTask.startAsyncTask(null);
    }

    public interface OnResultCallBack {
        void callBack(Object data);
    }
}
