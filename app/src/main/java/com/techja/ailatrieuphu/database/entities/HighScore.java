package com.techja.ailatrieuphu.database.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = "HighScore", primaryKeys = {"ID"})
public class HighScore {
    @NonNull
    @ColumnInfo(name = "Name")
    private String name;
    @ColumnInfo(name = "Score")
    private int score;
    @ColumnInfo(name = "ID")
    private int id;

    public HighScore(@NonNull String name, int score, int id) {
        this.name = name;
        this.score = score;
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "HighScore{" +
                "name='" + name + '\'' +
                ", score=" + score +
                ", id=" + id +
                '}';
    }
}
