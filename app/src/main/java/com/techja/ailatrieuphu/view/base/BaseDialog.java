package com.techja.ailatrieuphu.view.base;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;

public abstract class BaseDialog<T extends ViewBinding> extends Dialog
        implements View.OnClickListener {
    protected T mBinding;

    public BaseDialog(@NonNull Context context) {
        super(context);
        View view = LayoutInflater.from(context).inflate(getLayoutId(), null);
        mBinding = initViewBinding(view);
        setContentView(view);
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        initViews();
    }

    @Override
    public final void onClick(View v) {
        clickView(v);
    }

    protected void clickView(View v) {
        // do something
    }

    protected abstract void initViews();

    protected abstract T initViewBinding(View view);

    protected abstract int getLayoutId();

}
