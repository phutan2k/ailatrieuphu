package com.techja.ailatrieuphu.view.fragment;

import android.os.Handler;
import android.view.View;

import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.databinding.FrgM000SplashBinding;
import com.techja.ailatrieuphu.view.base.BaseFragment;
import com.techja.ailatrieuphu.viewmodel.CommonVM;

public class M000SplashFrg extends BaseFragment<FrgM000SplashBinding, CommonVM> {
    public static final String TAG = M000SplashFrg.class.getName();

    @Override
    protected void initViews() {
        new Handler().postDelayed(() -> mCallBack.showFrg(M001HomeFrg.TAG, null, false), 4000);
    }

    @Override
    protected Class<CommonVM> initClassVM() {
        return CommonVM.class;
    }

    @Override
    protected FrgM000SplashBinding initViewBinding(View view) {
        return FrgM000SplashBinding.bind(view);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m000_splash;
    }
}
