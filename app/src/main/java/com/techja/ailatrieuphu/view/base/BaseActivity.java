package com.techja.ailatrieuphu.view.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewbinding.ViewBinding;

import com.techja.ailatrieuphu.App;
import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.Storage;
import com.techja.ailatrieuphu.view.callback.OnHomeCallBack;
import com.techja.ailatrieuphu.viewmodel.BaseViewModel;

import java.lang.reflect.Constructor;

public abstract class BaseActivity<T extends ViewBinding, V extends BaseViewModel>
        extends AppCompatActivity implements View.OnClickListener, OnHomeCallBack {
    protected T mBinding;
    protected V mViewModel;

    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = LayoutInflater.from(this).inflate(getLayoutId(), null);
        mBinding = initViewBinding(view);
        mViewModel = new ViewModelProvider(this).get(initClassVM());
        setContentView(view);

        initViews();
    }

    @Override
    public final void onClick(View v) {
        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.abc_fade_in));
        clickView(v);
    }

    protected void clickView(View v) {
        // do something
    }

    protected final void notify(String sms) {
        Toast.makeText(this, sms, Toast.LENGTH_SHORT).show();
    }

    protected final void notify(int sms) {
        Toast.makeText(this, sms, Toast.LENGTH_SHORT).show();
    }

    protected abstract void initViews();

    protected abstract Class<V> initClassVM();

    protected abstract T initViewBinding(View view);

    protected abstract int getLayoutId();

    protected final Storage getStorage() {
        return App.getInstance().getStorage();
    }

    @Override
    public void showFrg(String key, Object data, boolean isBacked) {
        try {
            Class<?> clazz = Class.forName(key);
            Constructor<?> constructor = clazz.getConstructor();
            BaseFragment<?, ?> frg = (BaseFragment<?, ?>) constructor.newInstance();

            frg.setCallBack(this);
            frg.setData(data);

            FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
            if (isBacked) {
                trans.addToBackStack(null);
            }
            trans.replace(R.id.ln_main, frg).commit();

        } catch (Exception e) {
            notify("Error: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
