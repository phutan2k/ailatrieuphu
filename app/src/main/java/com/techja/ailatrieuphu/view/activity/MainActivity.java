package com.techja.ailatrieuphu.view.activity;

import android.util.Log;
import android.view.View;

import com.techja.ailatrieuphu.App;
import com.techja.ailatrieuphu.MediaManager;
import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.database.entities.Question;
import com.techja.ailatrieuphu.databinding.ActivityMainBinding;
import com.techja.ailatrieuphu.view.base.BaseActivity;
import com.techja.ailatrieuphu.view.fragment.M000SplashFrg;
import com.techja.ailatrieuphu.viewmodel.CommonVM;

import java.util.List;

public class MainActivity extends BaseActivity<ActivityMainBinding, CommonVM> {
    public static final String TAG = MainActivity.class.getName();

    @Override
    protected void initViews() {
        showFrg(M000SplashFrg.TAG, null, false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        MediaManager.getInstance().pauseSong();
    }

    @Override
    protected void onStart() {
        super.onStart();
        MediaManager.getInstance().playSong();
    }

    @Override
    protected Class<CommonVM> initClassVM() {
        return CommonVM.class;
    }

    @Override
    protected ActivityMainBinding initViewBinding(View view) {
        return ActivityMainBinding.bind(view);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }
}