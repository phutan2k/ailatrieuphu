package com.techja.ailatrieuphu.view.fragment;

import android.media.MediaPlayer;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;

import com.techja.ailatrieuphu.MediaManager;
import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.databinding.FrgM002RuleBinding;
import com.techja.ailatrieuphu.view.base.BaseFragment;
import com.techja.ailatrieuphu.view.dialog.CurrentQuestionDialog;
import com.techja.ailatrieuphu.view.dialog.InformReadyDialog;
import com.techja.ailatrieuphu.viewmodel.M002RuleVM;

public class M002RuleFrg extends BaseFragment<FrgM002RuleBinding, M002RuleVM> {
    public static final String TAG = M002RuleFrg.class.getName();

    @Override
    protected void initViews() {
        initSong();
        ruleAnimation();
        mBinding.btSkip.setOnClickListener(this);
    }

    private void initSong() {
        MediaManager.getInstance().playGame(R.raw.song_rule, new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                MediaManager.getInstance().playGame(R.raw.song_ready, new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        showReadyDialog();
                    }
                });
            }
        });
    }

    private void showReadyDialog() {
        InformReadyDialog dialog = new InformReadyDialog(mContext);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
        dialog.setCallBack(new InformReadyDialog.OnReadyCallBack() {
            @Override
            public void callBack(String key) {
                if (key.equals(InformReadyDialog.KEY_CANCEL)) {
                    doBack();
                } else if (key.equals(InformReadyDialog.KEY_READY)) {
                    getReady();
                }
            }
        });
    }

    private void getReady() {
        MediaManager.getInstance().stopPlayGame();
        mCallBack.showFrg(M003ReadyFrg.TAG, null, false);

        MediaManager.getInstance().playGame(R.raw.song_get_ready, new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                MediaManager.getInstance().playGame(R.raw.song_ques1, new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mCallBack.showFrg(M004GameFrg.TAG, null, false);
                        MediaManager.getInstance().playBG(R.raw.song_background);
                    }
                });
            }
        });
    }

    private void doBack() {
        MediaManager.getInstance().stopPlayGame();
        mCallBack.showFrg(M001HomeFrg.TAG, null, false);
    }

    private void ruleAnimation() {
        mBinding.lnMilestone.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.slide_left));
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.bt_skip) {
            MediaManager.getInstance().stopPlayGame();
            showReadyDialog();
        }
    }

    @Override
    protected Class<M002RuleVM> initClassVM() {
        return M002RuleVM.class;
    }

    @Override
    protected FrgM002RuleBinding initViewBinding(View view) {
        return FrgM002RuleBinding.bind(view);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m002_rule;
    }
}
