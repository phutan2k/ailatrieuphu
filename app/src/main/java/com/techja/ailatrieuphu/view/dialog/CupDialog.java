package com.techja.ailatrieuphu.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.database.DBManager;
import com.techja.ailatrieuphu.database.entities.HighScore;

import java.util.List;

public class CupDialog extends Dialog implements View.OnClickListener {
    private final OnCupCallBack callBack;
    private final Context mContext;
    private TextView tvName1, tvName2, tvName3, tvMoneyTop1, tvMoneyTop2, tvMoneyTop3;

    public CupDialog(@NonNull Context context, OnCupCallBack callBack) {
        super(context);
        mContext = context;
        this.callBack = callBack;
        setContentView(R.layout.view_cup);
        setCancelable(false);

        initViews();
    }

    private void initViews() {
        findViewById(R.id.bt_back_cup).setOnClickListener(this);
        tvName1 = findViewById(R.id.tv_user_top_1);
        tvName2 = findViewById(R.id.tv_user_top_2);
        tvName3 = findViewById(R.id.tv_user_top_3);
        tvMoneyTop1 = findViewById(R.id.tv_money_top_1);
        tvMoneyTop2 = findViewById(R.id.tv_money_top_2);
        tvMoneyTop3 = findViewById(R.id.tv_money_top_3);

        initData();
    }

    private void initData() {
        DBManager.getInstance().getListHighScore(new DBManager.OnResultCallBack() {
            @Override
            public void callBack(Object data) {
                List<HighScore> listScore = (List<HighScore>) data;
                int i = 0;
                int scoreTop1 = 0;
                int scoreTop2 = 0;
                int scoreTop3 = 0;

                while (i < listScore.size()) {
                    if (i == 0) {
                        tvName1.setText(listScore.get(0).getName());
                        scoreTop1 = listScore.get(0).getScore();
                    } else if (i == 1) {
                        tvName2.setText(listScore.get(1).getName());
                        scoreTop2 = listScore.get(1).getScore();
                    } else if (i == 2) {
                        tvName3.setText(listScore.get(2).getName());
                        scoreTop3 = listScore.get(2).getScore();
                    }
                    i++;
                }
                getScore(scoreTop1, tvMoneyTop1);
                getScore(scoreTop2, tvMoneyTop2);
                getScore(scoreTop3, tvMoneyTop3);
            }
        });
    }

    private void getScore(int score, TextView tvMoney) {
        switch (score){
            case 1:
                tvMoney.setText(R.string.txt_200_000);
                break;

            case 2:
                tvMoney.setText(R.string.txt_400_000);
                break;

            case 3:
                tvMoney.setText(R.string.txt_600_000);
                break;

            case 4:
                tvMoney.setText(R.string.txt_200_000);
                break;

            case 5:
                tvMoney.setText(R.string.txt_2_000_000);
                break;

            case 6:
                tvMoney.setText(R.string.txt_3_000_000);
                break;

            case 7:
                tvMoney.setText(R.string.txt_6_000_000);
                break;

            case 8:
                tvMoney.setText(R.string.txt_10_000_000);
                break;

            case 9:
                tvMoney.setText(R.string.txt_14_000_000);
                break;

            case 10:
                tvMoney.setText(R.string.txt_22_000_000);
                break;

            case 11:
                tvMoney.setText(R.string.txt_30_000_000);
                break;

            case 12:
                tvMoney.setText(R.string.txt_40_000_000);
                break;

            case 13:
                tvMoney.setText(R.string.txt_60_000_000);
                break;

            case 14:
                tvMoney.setText(R.string.txt_85_000_000);
                break;

            case 15:
                tvMoney.setText(R.string.txt_150_000_000);
                break;

            default:
                tvMoney.setText("0");
                break;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_back_cup) {
            dismiss();
        }
    }

    public interface OnCupCallBack {
        void callBack();
    }
}
