package com.techja.ailatrieuphu.view.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewbinding.ViewBinding;

import com.techja.ailatrieuphu.view.callback.OnHomeCallBack;
import com.techja.ailatrieuphu.viewmodel.BaseViewModel;

public abstract class BaseFragment<T extends ViewBinding, V extends BaseViewModel>
        extends Fragment implements View.OnClickListener {
    protected T mBinding;
    protected V mViewModel;
    protected Context mContext;
    protected Object mData;
    protected OnHomeCallBack mCallBack;

    public void setCallBack(OnHomeCallBack mCallBack) {
        this.mCallBack = mCallBack;
    }

    public void setData(Object mData) {
        this.mData = mData;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public final View onCreateView(@NonNull LayoutInflater inflater,
                                   @Nullable ViewGroup container,
                                   @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        mBinding = initViewBinding(view);
        mViewModel = new ViewModelProvider(this).get(initClassVM());

        initViews();
        return view;
    }

    @Override
    public final void onClick(View v) {
        clickView(v);
    }

    protected void clickView(View v) {
        // do something
    }

    protected final void notify(String sms) {
        Toast.makeText(mContext, sms, Toast.LENGTH_SHORT).show();
    }

    protected final void notify(int sms) {
        Toast.makeText(mContext, sms, Toast.LENGTH_SHORT).show();
    }

    protected abstract void initViews();

    protected abstract Class<V> initClassVM();

    protected abstract T initViewBinding(View view);

    protected abstract int getLayoutId();
}
