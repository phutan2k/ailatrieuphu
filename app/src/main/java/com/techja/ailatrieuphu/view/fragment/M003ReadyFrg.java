package com.techja.ailatrieuphu.view.fragment;

import android.view.View;

import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.databinding.FrgM003ReadyBinding;
import com.techja.ailatrieuphu.view.base.BaseFragment;
import com.techja.ailatrieuphu.viewmodel.M003ReadyVM;

public class M003ReadyFrg extends BaseFragment<FrgM003ReadyBinding, M003ReadyVM> {
    public static final String TAG = M003ReadyFrg.class.getName();

    @Override
    protected void initViews() {

    }

    @Override
    protected Class<M003ReadyVM> initClassVM() {
        return M003ReadyVM.class;
    }

    @Override
    protected FrgM003ReadyBinding initViewBinding(View view) {
        return FrgM003ReadyBinding.bind(view);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m003_ready;
    }
}
