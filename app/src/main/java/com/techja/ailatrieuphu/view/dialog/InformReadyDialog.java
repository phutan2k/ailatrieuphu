package com.techja.ailatrieuphu.view.dialog;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.databinding.ViewReadyBinding;
import com.techja.ailatrieuphu.view.base.BaseDialog;

public class InformReadyDialog extends BaseDialog<ViewReadyBinding> {
    public static final String KEY_READY = "KEY_READY";
    public static final String KEY_CANCEL = "KEY_CANCEL";
    private OnReadyCallBack callBack;

    public InformReadyDialog(@NonNull Context context) {
        super(context);
    }

    public void setCallBack(OnReadyCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    protected void initViews() {
        mBinding.btReady.setOnClickListener(this);
        mBinding.btBack.setOnClickListener(this);
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.bt_ready) {
            getReady();
        } else if (v.getId() == R.id.bt_back) {
            doBack();
        }
    }

    private void doBack() {
        callBack.callBack(KEY_CANCEL);
        dismiss();
    }

    private void getReady() {
        callBack.callBack(KEY_READY);
        dismiss();
    }

    @Override
    protected ViewReadyBinding initViewBinding(View view) {
        return ViewReadyBinding.bind(view);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_ready;
    }

    public interface OnReadyCallBack {
        void callBack(String key);
    }
}
