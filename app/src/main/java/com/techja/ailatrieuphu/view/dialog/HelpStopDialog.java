package com.techja.ailatrieuphu.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.techja.ailatrieuphu.App;
import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.database.DBManager;
import com.techja.ailatrieuphu.database.entities.HighScore;

import java.util.Random;

public class HelpStopDialog extends Dialog implements View.OnClickListener {
    private static final String TAG = HelpStopDialog.class.getName();
    private final Context mContext;
    private final OnHelpStop callBack;
    private EditText edtName;

    public HelpStopDialog(@NonNull Context context, OnHelpStop callBack) {
        super(context);
        mContext = context;
        this.callBack = callBack;
        setContentView(R.layout.view_help_stop);
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        initViews();
    }

    private void initViews() {
        findViewById(R.id.bt_cancel).setOnClickListener(this);
        findViewById(R.id.bt_accept).setOnClickListener(this);
        edtName = findViewById(R.id.edt_name_user);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_cancel) {
            dismiss();
        } else if (v.getId() == R.id.bt_accept) {
            dismiss();
            saveScore();
            callBack.callBack();
        }
    }

    private void saveScore() {
        String name = edtName.getText().toString();
        if (name.isEmpty()) {
            Toast.makeText(mContext, "Empty value!", Toast.LENGTH_SHORT).show();
        }
        int score = App.getInstance().getStorage().getQuestionLevel();
        int id = new Random().nextInt(10000) + 1000;

        HighScore highScore = new HighScore(name, score, id);
        Log.i(HelpStopDialog.TAG, highScore.toString());

        DBManager.getInstance().addScore(highScore, new DBManager.OnResultCallBack() {
            @Override
            public void callBack(Object data) {
                Toast.makeText(mContext, (boolean) data ? "Success" : "False", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public interface OnHelpStop {
        void callBack();
    }
}
