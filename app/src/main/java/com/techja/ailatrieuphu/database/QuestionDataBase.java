package com.techja.ailatrieuphu.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.techja.ailatrieuphu.database.dao.QuestionDAO;
import com.techja.ailatrieuphu.database.entities.HighScore;
import com.techja.ailatrieuphu.database.entities.Question;

@Database(version = 1, entities = {HighScore.class, Question.class})
public abstract class QuestionDataBase extends RoomDatabase {
    public abstract QuestionDAO getQuestionDAO();
}
