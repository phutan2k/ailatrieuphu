package com.techja.ailatrieuphu.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.techja.ailatrieuphu.R;

public class HelpCallAnswerDialog extends Dialog {
    private final OnHelpCallAnswer callBack;
    private ImageView ivAvatar;
    private TextView tvNickName, tvAnswer;

    public HelpCallAnswerDialog(@NonNull Context context, OnHelpCallAnswer callBack) {
        super(context);
        setContentView(R.layout.view_help_call_answer);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        this.callBack = callBack;

        initViews();
    }

    private void initViews() {
        ivAvatar = findViewById(R.id.iv_avatar);
        tvNickName = findViewById(R.id.tv_nick_name);
        tvAnswer = findViewById(R.id.tv_answer);
        findViewById(R.id.tv_answer_help_call_close).setOnClickListener(v -> {
            if (v.getId() == R.id.tv_answer_help_call_close) {
                dismiss();
                callBack.callBack();
            }
        });
    }

    public void updateInfo(int image, String nickName, String answer) {
        ivAvatar.setImageResource(image);
        tvNickName.setText(nickName);
        tvAnswer.setText(answer);
    }

    public interface OnHelpCallAnswer {
        void callBack();
    }
}
