package com.techja.ailatrieuphu.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.techja.ailatrieuphu.database.entities.HighScore;
import com.techja.ailatrieuphu.database.entities.Question;

import java.util.List;

@Dao
public interface QuestionDAO {
    @Query("SELECT * FROM (SELECT * FROM (SELECT * FROM question WHERE level = 1 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 2 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 3 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 4 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 5 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 6 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 7 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 8 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 9 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 10 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 11 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 12 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 13 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 14 ORDER by random() LIMIT 1)\n" +
            "UNION SELECT * FROM (SELECT * FROM question WHERE level = 15 ORDER by random() LIMIT 1)\n" +
            ")ORDER by level ASC")
    List<Question> getAllQuestionInOneTurn();

    @Query("SELECT * FROM question WHERE level = :level ORDER by random() LIMIT 1")
    Question getQuestionForLevel(int level);

    @Query("SELECT * FROM HighScore ORDER BY Score DESC LIMIT 3")
    List<HighScore> getListHighScore();

    @Insert
    void addScore(HighScore... highScore);
}
