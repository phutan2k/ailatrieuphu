package com.techja.ailatrieuphu.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.techja.ailatrieuphu.App;
import com.techja.ailatrieuphu.CommonUtils;
import com.techja.ailatrieuphu.MediaManager;
import com.techja.ailatrieuphu.R;

public class SettingDialog extends Dialog implements View.OnClickListener {
    public static final String KEY_SONG_STATE = "KEY_SONG_STATE";
    public static final String PLAYING = "PLAYING";
    public static final String PAUSE = "PAUSE";
    private final OnSettingCallBack callBack;
    private final Context mContext;
    private ImageView ivMusicSet;

    public SettingDialog(@NonNull Context context, OnSettingCallBack callBack) {
        super(context);
        mContext = context;
        this.callBack = callBack;
        setContentView(R.layout.view_setting);
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        initViews();
    }

    private void initViews() {
        findViewById(R.id.bt_back_setting).setOnClickListener(this);
        ivMusicSet = findViewById(R.id.iv_music_set);
        ivMusicSet.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_back_setting) {
            dismiss();
        }else if(v.getId() == R.id.iv_music_set){
            if (ivMusicSet.getDrawable().getLevel() == 0) {
                MediaManager.getInstance().pauseSong();
                ivMusicSet.getDrawable().setLevel(1);
                CommonUtils.getInstance().savePref(KEY_SONG_STATE, PAUSE);
            } else {
                MediaManager.getInstance().playSong();
                ivMusicSet.getDrawable().setLevel(0);
                CommonUtils.getInstance().savePref(KEY_SONG_STATE, PLAYING);
            }
        }
    }

    public interface OnSettingCallBack {
        void callBack();
    }
}
