package com.techja.ailatrieuphu.view.fragment;

import android.view.View;
import android.view.WindowManager;

import com.techja.ailatrieuphu.App;
import com.techja.ailatrieuphu.CommonUtils;
import com.techja.ailatrieuphu.MediaManager;
import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.databinding.FrgM001HomeBinding;
import com.techja.ailatrieuphu.view.base.BaseFragment;
import com.techja.ailatrieuphu.view.dialog.CupDialog;
import com.techja.ailatrieuphu.view.dialog.InfoDialog;
import com.techja.ailatrieuphu.view.dialog.SettingDialog;
import com.techja.ailatrieuphu.viewmodel.M001LoginVM;

public class M001HomeFrg extends BaseFragment<FrgM001HomeBinding, M001LoginVM> {
    public static final String TAG = M001HomeFrg.class.getName();

    @Override
    protected void initViews() {
        App.getInstance().getStorage().setQuestionLevel(1);
        App.getInstance().getStorage().setMoneyLevel(0);

        MediaManager.getInstance().playBG(R.raw.song_intro);

        mBinding.ivStartGame.setOnClickListener(this);
        mBinding.ivIntro.setOnClickListener(this);
        mBinding.ivCup.setOnClickListener(this);
        mBinding.ivSetting.setOnClickListener(this);

        initSongState();
    }

    private void initSongState() {
        String stateSong = CommonUtils.getInstance().getPref(SettingDialog.KEY_SONG_STATE);
        if (stateSong == null) {
            return;
        }
        if (stateSong.equals(SettingDialog.PLAYING)) {
            MediaManager.getInstance().playSong();
        } else {
            MediaManager.getInstance().pauseSong();
        }
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.iv_start_game) {
            startGame();
        } else if (v.getId() == R.id.iv_intro) {
            getInto();
        } else if (v.getId() == R.id.iv_cup) {
            getCup();
        } else if (v.getId() == R.id.iv_setting) {
            getSetting();
        }
    }

    private void getSetting() {
        SettingDialog dialog = new SettingDialog(mContext, () -> {
        });
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void getCup() {
        CupDialog dialog = new CupDialog(mContext, () -> {
        });
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void getInto() {
        InfoDialog dialog = new InfoDialog(mContext, () -> {
        });
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void startGame() {
        mCallBack.showFrg(M002RuleFrg.TAG, null, false);
        MediaManager.getInstance().stopBG();
    }

    @Override
    protected Class<M001LoginVM> initClassVM() {
        return M001LoginVM.class;
    }

    @Override
    protected FrgM001HomeBinding initViewBinding(View view) {
        return FrgM001HomeBinding.bind(view);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m001_home;
    }
}
