package com.techja.ailatrieuphu;

import androidx.lifecycle.MutableLiveData;

public final class Storage {
    private final MutableLiveData<Integer> moneyLevel = new MutableLiveData<>(0);
    private final MutableLiveData<Integer> trueCase = new MutableLiveData<>();
    private int questionLevel;

    public Storage() {
        questionLevel = 1;
    }

    public int getQuestionLevel() {
        return questionLevel;
    }

    public void setQuestionLevel(int questionLevel) {
        this.questionLevel = questionLevel;
    }

    public MutableLiveData<Integer> getMoneyLevel() {
        return moneyLevel;
    }

    public void setMoneyLevel(int moneyLevel) {
        this.moneyLevel.setValue(moneyLevel);
    }

    public MutableLiveData<Integer> getTrueCase() {
        return trueCase;
    }

    public void setTrueCase(int trueCase) {
        this.trueCase.setValue(trueCase);
    }
}
