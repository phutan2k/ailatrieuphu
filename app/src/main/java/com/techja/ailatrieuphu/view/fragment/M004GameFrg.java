package com.techja.ailatrieuphu.view.fragment;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.lifecycle.Observer;

import com.techja.ailatrieuphu.App;
import com.techja.ailatrieuphu.MTask;
import com.techja.ailatrieuphu.MediaManager;
import com.techja.ailatrieuphu.R;
import com.techja.ailatrieuphu.database.DBManager;
import com.techja.ailatrieuphu.database.entities.Question;
import com.techja.ailatrieuphu.databinding.FrgM004GameBinding;
import com.techja.ailatrieuphu.view.activity.MainActivity;
import com.techja.ailatrieuphu.view.base.BaseFragment;
import com.techja.ailatrieuphu.view.dialog.CurrentQuestionDialog;
import com.techja.ailatrieuphu.view.dialog.HelpCallDialog;
import com.techja.ailatrieuphu.view.dialog.HelpStopDialog;
import com.techja.ailatrieuphu.view.dialog.TimeOutDialog;
import com.techja.ailatrieuphu.viewmodel.M004GameVM;

import java.util.Random;

public class M004GameFrg extends BaseFragment<FrgM004GameBinding, M004GameVM> implements MTask.MTaskListener {
    public static final String TAG = M004GameFrg.class.getName();
    private static final String KEY_TASK_COUNTING = "KEY_TASK_COUNTING";
    private int trueCase;
    private MTask taskTime;

    @Override
    protected void initViews() {
        mBinding.tvAnswerA.setOnClickListener(this);
        mBinding.tvAnswerB.setOnClickListener(this);
        mBinding.tvAnswerC.setOnClickListener(this);
        mBinding.tvAnswerD.setOnClickListener(this);
        mBinding.ivHelpCall.setOnClickListener(this);
        mBinding.ivHelpAudience.setOnClickListener(this);
        mBinding.ivHelp5050.setOnClickListener(this);
        mBinding.ivChangeQuestion.setOnClickListener(this);
        mBinding.ivHelpStop.setOnClickListener(this);

        initData(App.getInstance().getStorage().getQuestionLevel());
        updateMoney();
        updateTime();
    }

    private void initData(int i) {
        DBManager.getInstance().getQuestionForLevel(i, new DBManager.OnResultCallBack() {
            @Override
            public void callBack(Object data) {
                Question question = (Question) data;

                MainActivity act = (MainActivity) mContext;
                act.runOnUiThread(new Runnable() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run() {
                        mBinding.tvQuestionNumber.setText("Câu " + i);
                        mBinding.tvQuestion.setText(question.getQuestion());
                        mBinding.tvAnswerA.setText(question.getCaseA());
                        mBinding.tvAnswerB.setText(question.getCaseB());
                        mBinding.tvAnswerC.setText(question.getCaseC());
                        mBinding.tvAnswerD.setText(question.getCaseD());

                        trueCase = question.getTrueCase();
                        App.getInstance().getStorage().setTrueCase(trueCase);
                        Log.i(TAG, "Đáp án: " + trueCase);

                        mBinding.tvAnswerA.setBackgroundResource(R.drawable.bg_answer_normal);
                        mBinding.tvAnswerB.setBackgroundResource(R.drawable.bg_answer_normal);
                        mBinding.tvAnswerC.setBackgroundResource(R.drawable.bg_answer_normal);
                        mBinding.tvAnswerD.setBackgroundResource(R.drawable.bg_answer_normal);

                        mBinding.tvAnswerA.setEnabled(true);
                        mBinding.tvAnswerB.setEnabled(true);
                        mBinding.tvAnswerC.setEnabled(true);
                        mBinding.tvAnswerD.setEnabled(true);
                    }
                });
            }
        });
    }

    @Override
    protected void clickView(View v) {
        if (v.getId() == R.id.tv_answer_a) {
            taskTime.stopAsync();
            checkAnswer(R.raw.song_ans_a, mBinding.tvAnswerA, 1);
        } else if (v.getId() == R.id.tv_answer_b) {
            taskTime.stopAsync();
            checkAnswer(R.raw.song_ans_b, mBinding.tvAnswerB, 2);
        } else if (v.getId() == R.id.tv_answer_c) {
            taskTime.stopAsync();
            checkAnswer(R.raw.song_ans_c, mBinding.tvAnswerC, 3);
        } else if (v.getId() == R.id.tv_answer_d) {
            taskTime.stopAsync();
            checkAnswer(R.raw.song_ans_d, mBinding.tvAnswerD, 4);
        } else if (v.getId() == R.id.iv_help_call) {
            helpCall();
        } else if (v.getId() == R.id.iv_help_audience) {
            helpAudience();
        } else if (v.getId() == R.id.iv_help_50_50) {
            helpFifty();
        } else if (v.getId() == R.id.iv_change_question) {
            helpChangeQuestion();
        } else if (v.getId() == R.id.iv_help_stop) {
            taskTime.stopAsync();
            helpStop();
        }
    }

    private void helpStop() {
        HelpStopDialog dialog = new HelpStopDialog(mContext, new HelpStopDialog.OnHelpStop() {
            @Override
            public void callBack() {
                mCallBack.showFrg(M001HomeFrg.TAG, null, false);
            }
        });
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void helpChangeQuestion() {
        if (mBinding.ivChangeQuestion.getDrawable().getLevel() == 0) {
            mBinding.ivChangeQuestion.setImageLevel(1);
            mBinding.ivChangeQuestion.setEnabled(false);
        }
        initData(App.getInstance().getStorage().getQuestionLevel());
    }

    private void helpFifty() {
        if (mBinding.ivHelp5050.getDrawable().getLevel() == 0) {
            mBinding.ivHelp5050.setImageLevel(1);
            mBinding.ivHelp5050.setEnabled(false);

            MediaManager.getInstance().playGame(R.raw.song_help_5050, new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    handleHelp5050();
                }
            });
        }
    }

    private void handleHelp5050() {
        if (trueCase == 1) {
            Random rd = new Random();
            int answer = rd.nextInt(3) + 2;
            switch (answer) {
                case 2:
                    mBinding.tvAnswerC.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerC.setEnabled(false);
                    mBinding.tvAnswerD.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerD.setEnabled(false);
                    break;

                case 3:
                    mBinding.tvAnswerB.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerB.setEnabled(false);
                    mBinding.tvAnswerD.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerD.setEnabled(false);
                    break;

                case 4:
                    mBinding.tvAnswerB.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerB.setEnabled(false);
                    mBinding.tvAnswerC.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerC.setEnabled(false);
                    break;

                default:
                    break;
            }
        } else if (trueCase == 2) {
            Random rd = new Random();
            int answer = rd.nextInt(3) + 2;
            switch (answer) {
                case 2:
                    mBinding.tvAnswerC.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerC.setEnabled(false);
                    mBinding.tvAnswerD.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerD.setEnabled(false);
                    break;

                case 3:
                    mBinding.tvAnswerA.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerA.setEnabled(false);
                    mBinding.tvAnswerD.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerD.setEnabled(false);
                    break;

                case 4:
                    mBinding.tvAnswerA.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerA.setEnabled(false);
                    mBinding.tvAnswerC.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerC.setEnabled(false);
                    break;

                default:
                    break;
            }
        } else if (trueCase == 3) {
            Random rd = new Random();
            int answer = rd.nextInt(3) + 2;
            switch (answer) {
                case 2:
                    mBinding.tvAnswerA.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerA.setEnabled(false);
                    mBinding.tvAnswerD.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerD.setEnabled(false);
                    break;

                case 3:
                    mBinding.tvAnswerB.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerB.setEnabled(false);
                    mBinding.tvAnswerD.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerD.setEnabled(false);
                    break;

                case 4:
                    mBinding.tvAnswerA.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerA.setEnabled(false);
                    mBinding.tvAnswerB.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerB.setEnabled(false);
                    break;

                default:
                    break;
            }
        } else if (trueCase == 4) {
            Random rd = new Random();
            int answer = rd.nextInt(3) + 2;
            switch (answer) {
                case 2:
                    mBinding.tvAnswerC.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerC.setEnabled(false);
                    mBinding.tvAnswerA.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerA.setEnabled(false);
                    break;

                case 3:
                    mBinding.tvAnswerB.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerB.setEnabled(false);
                    mBinding.tvAnswerA.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerA.setEnabled(false);
                    break;

                case 4:
                    mBinding.tvAnswerC.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerC.setEnabled(false);
                    mBinding.tvAnswerB.setVisibility(View.INVISIBLE);
                    mBinding.tvAnswerB.setEnabled(false);
                    break;

                default:
                    break;
            }
        }
    }

    private void helpAudience() {
        //TODO
    }

    private void helpCall() {
        if (mBinding.ivHelpCall.getDrawable().getLevel() == 0) {
            mBinding.ivHelpCall.setImageLevel(1);
            mBinding.ivHelpCall.setEnabled(false);
        }

        MediaPlayer mediaPlayer = MediaPlayer.create(mContext, R.raw.song_help_call);
        mediaPlayer.start();
        showHelpCallDialog();
    }

    private void showHelpCallDialog() {
        HelpCallDialog dialog = new HelpCallDialog(mContext, new HelpCallDialog.OnHelpCall() {
            @Override
            public void callBack() {

            }
        });
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void showCurrentQuestionDialog() {
        CurrentQuestionDialog dialog = new CurrentQuestionDialog(mContext);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void checkAnswer(int idSong, TextView tvSelected, int idTvSelected) {
        tvSelected.setBackgroundResource(R.drawable.bg_answer_selected);
        enableClick(false);
        MediaManager.getInstance().playGame(idSong, new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                switch (trueCase) {
                    case 1:
                        if (idTvSelected == 1) {
                            answerTrue(R.raw.song_true_a, mBinding.tvAnswerA);
                        } else if (idTvSelected == 2) {
                            answerWrong(R.raw.song_lose_a, mBinding.tvAnswerA, mBinding.tvAnswerB);
                        } else if (idTvSelected == 3) {
                            answerWrong(R.raw.song_lose_a, mBinding.tvAnswerA, mBinding.tvAnswerC);
                        } else if (idTvSelected == 4) {
                            answerWrong(R.raw.song_lose_a, mBinding.tvAnswerA, mBinding.tvAnswerD);
                        }
                        break;

                    case 2:
                        if (idTvSelected == 2) {
                            answerTrue(R.raw.song_true_b, mBinding.tvAnswerB);
                        } else if (idTvSelected == 1) {
                            answerWrong(R.raw.song_lose_b, mBinding.tvAnswerB, mBinding.tvAnswerA);
                        } else if (idTvSelected == 3) {
                            answerWrong(R.raw.song_lose_b, mBinding.tvAnswerB, mBinding.tvAnswerC);
                        } else if (idTvSelected == 4) {
                            answerWrong(R.raw.song_lose_b, mBinding.tvAnswerB, mBinding.tvAnswerD);
                        }
                        break;

                    case 3:
                        if (idTvSelected == 3) {
                            answerTrue(R.raw.song_true_c, mBinding.tvAnswerC);
                        } else if (idTvSelected == 1) {
                            answerWrong(R.raw.song_lose_c, mBinding.tvAnswerC, mBinding.tvAnswerA);
                        } else if (idTvSelected == 2) {
                            answerWrong(R.raw.song_lose_c, mBinding.tvAnswerC, mBinding.tvAnswerB);
                        } else if (idTvSelected == 4) {
                            answerWrong(R.raw.song_lose_c, mBinding.tvAnswerC, mBinding.tvAnswerD);
                        }
                        break;

                    case 4:
                        if (idTvSelected == 4) {
                            answerTrue(R.raw.song_true_d, mBinding.tvAnswerD);
                        } else if (idTvSelected == 1) {
                            answerWrong(R.raw.song_lose_d, mBinding.tvAnswerD, mBinding.tvAnswerA);
                        } else if (idTvSelected == 2) {
                            answerWrong(R.raw.song_lose_d, mBinding.tvAnswerD, mBinding.tvAnswerB);
                        } else if (idTvSelected == 3) {
                            answerWrong(R.raw.song_lose_d, mBinding.tvAnswerD, mBinding.tvAnswerC);
                        }
                        break;

                    default:
                        break;
                }
            }
        });
    }

    private void enableClick(boolean isEnable) {
        mBinding.tvAnswerA.setEnabled(isEnable);
        mBinding.tvAnswerB.setEnabled(isEnable);
        mBinding.tvAnswerC.setEnabled(isEnable);
        mBinding.tvAnswerD.setEnabled(isEnable);
    }


    private void answerWrong(int idSong, TextView tvTrue, TextView tvSelected) {
        tvTrue.setBackgroundResource(R.drawable.bg_answer_true);
        tvSelected.setBackgroundResource(R.drawable.bg_answer_wrong);
        tvSelected.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.blink));
        MediaManager.getInstance().playGame(idSong, new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                MediaManager.getInstance().playGame(R.raw.song_lose, new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mCallBack.showFrg(M001HomeFrg.TAG, null, false);
                    }
                });
            }
        });
    }

    private void answerTrue(int idSong, TextView tvSelected) {
        if (App.getInstance().getStorage().getQuestionLevel() >= 16) {
            mCallBack.showFrg(M001HomeFrg.TAG, null, false);
            return;
        }
        App.getInstance().getStorage().setQuestionLevel(App.getInstance().getStorage().getQuestionLevel() + 1);
        App.getInstance().getStorage().setMoneyLevel(App.getInstance().getStorage().getQuestionLevel());

        tvSelected.setBackgroundResource(R.drawable.bg_answer_true);
        tvSelected.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.blink));

        MediaManager.getInstance().playGame(idSong, new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                showCurrentQuestionDialog();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (App.getInstance().getStorage().getQuestionLevel() == 6) {
                            MediaManager.getInstance().playGame(R.raw.song_vuot_moc_1, new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    MediaManager.getInstance().playGame(R.raw.song_ques6, null);
                                    mCallBack.showFrg(M004GameFrg.TAG, null, false);
                                }
                            });
                        } else if (App.getInstance().getStorage().getQuestionLevel() == 11) {
                            MediaManager.getInstance().playGame(R.raw.song_vuot_moc_2, new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    MediaManager.getInstance().playGame(R.raw.song_ques11, null);
                                    mCallBack.showFrg(M004GameFrg.TAG, null, false);
                                }
                            });
                        } else {
                            mCallBack.showFrg(M004GameFrg.TAG, null, false);
                        }
                    }
                }, 2000);

                startMediaQuestion();
            }
        });
    }

    private void startMediaQuestion() {
        switch (App.getInstance().getStorage().getQuestionLevel()) {
            case 2:
                MediaManager.getInstance().playGame(R.raw.song_ques2, null);
                break;

            case 3:
                MediaManager.getInstance().playGame(R.raw.song_ques3, null);
                break;

            case 4:
                MediaManager.getInstance().playGame(R.raw.song_ques4, null);
                break;

            case 5:
                MediaManager.getInstance().playGame(R.raw.song_ques5, new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        MediaManager.getInstance().playGame(R.raw.song_important, null);
                    }
                });
                break;

            case 7:
                MediaManager.getInstance().playGame(R.raw.song_ques7, null);
                break;

            case 8:
                MediaManager.getInstance().playGame(R.raw.song_ques8, null);
                break;

            case 9:
                MediaManager.getInstance().playGame(R.raw.song_ques9, null);
                break;

            case 10:
                MediaManager.getInstance().playGame(R.raw.song_ques10, new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        MediaManager.getInstance().playGame(R.raw.song_important, null);
                    }
                });
                break;

            case 12:
                MediaManager.getInstance().playGame(R.raw.song_ques12, null);
                break;

            case 13:
                MediaManager.getInstance().playGame(R.raw.song_ques13, null);
                break;

            case 14:
                MediaManager.getInstance().playGame(R.raw.song_ques14, null);
                break;

            case 15:
                MediaManager.getInstance().playGame(R.raw.song_ques15, null);
                break;

            default:
                break;
        }
    }

    private void updateMoney() {
        App.getInstance().getStorage().getMoneyLevel().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer moneyLevel) {
                update(moneyLevel);
            }
        });
    }

    private void update(Integer moneyLevel) {
        switch (moneyLevel) {
            case 0:
                mBinding.tvMoney.setText("0");
                break;

            case 2:
                mBinding.tvMoney.setText(R.string.txt_200_000);
                break;

            case 3:
                mBinding.tvMoney.setText(R.string.txt_400_000);
                break;

            case 4:
                mBinding.tvMoney.setText(R.string.txt_600_000);
                break;

            case 5:
                mBinding.tvMoney.setText(R.string.txt_1_000_000);
                break;

            case 6:
                mBinding.tvMoney.setText(R.string.txt_2_000_000);
                break;

            case 7:
                mBinding.tvMoney.setText(R.string.txt_3_000_000);
                break;

            case 8:
                mBinding.tvMoney.setText(R.string.txt_6_000_000);
                break;

            case 9:
                mBinding.tvMoney.setText(R.string.txt_10_000_000);
                break;

            case 10:
                mBinding.tvMoney.setText(R.string.txt_14_000_000);
                break;

            case 11:
                mBinding.tvMoney.setText(R.string.txt_22_000_000);
                break;

            case 12:
                mBinding.tvMoney.setText(R.string.txt_30_000_000);
                break;

            case 13:
                mBinding.tvMoney.setText(R.string.txt_40_000_000);
                break;


            case 14:
                mBinding.tvMoney.setText(R.string.txt_60_000_000);
                break;

            case 15:
                mBinding.tvMoney.setText(R.string.txt_85_000_000);
                break;

            case 16:
                mBinding.tvMoney.setText(R.string.txt_150_000_000);
                break;
        }
    }

    private void updateTime() {
        if (taskTime == null) {
            taskTime = new MTask(KEY_TASK_COUNTING, this);
            taskTime.startAsyncTask(30);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void startExec() {
        mBinding.tvTime.setText("30");
    }

    @Override
    public Object execTask(Object dataInput, String key, MTask task) {
        return doCounting(dataInput, task);
    }

    private Object doCounting(Object dataInput, MTask task) {
        for (int i = (int) dataInput; i >= 0; i--) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                return false;
            }

            taskTime.requestUpdateUI(i);
        }
        return true;
    }

    @Override
    public void updateUI(Object dataUpdate, String key) {
        if (key.equals(KEY_TASK_COUNTING)) {
            updateCountingTV((int) dataUpdate);
        }
    }

    private void updateCountingTV(int dataUpdate) {
        mBinding.tvTime.setText(String.format("%s", dataUpdate));
    }

    @Override
    public void completeTask(Object result, String key) {
        if (key.equals(KEY_TASK_COUNTING)) {
            if ((boolean) result) {
                MediaManager.getInstance().stopBG();
                MediaManager.getInstance().playGame(R.raw.song_out_of_time, null);
                showTimeOutDialog();
            }
        }
    }

    private void showTimeOutDialog() {
        TimeOutDialog dialog = new TimeOutDialog(mContext);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
        dialog.setCallBack(new TimeOutDialog.OnTimeOutCallBack() {
            @Override
            public void callBack(String key) {
                if (key.equals(TimeOutDialog.KEY_TIME_OUT)) {
                    doTimeOut();
                }
            }
        });
    }

    private void doTimeOut() {
        MediaManager.getInstance().playGame(R.raw.song_lose, new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mCallBack.showFrg(M001HomeFrg.TAG, null, false);
            }
        });
    }

    @Override
    protected Class<M004GameVM> initClassVM() {
        return M004GameVM.class;
    }

    @Override
    protected FrgM004GameBinding initViewBinding(View view) {
        return FrgM004GameBinding.bind(view);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m004_game;
    }
}
