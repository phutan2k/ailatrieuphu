package com.techja.ailatrieuphu.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.techja.ailatrieuphu.App;
import com.techja.ailatrieuphu.MediaManager;
import com.techja.ailatrieuphu.R;

public class HelpCallDialog extends Dialog implements View.OnClickListener {
    private static final String TAG = HelpCallDialog.class.getName();
    private final Context mContext;
    private final OnHelpCall callBack;
    private String mAnswer;

    public HelpCallDialog(@NonNull Context context, OnHelpCall callBack) {
        super(context);
        mContext = context;
        setContentView(R.layout.view_help_call);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        this.callBack = callBack;

        initViews();
    }

    private void initViews() {
        findViewById(R.id.iv_help_call_1).setOnClickListener(this);
        findViewById(R.id.iv_help_call_2).setOnClickListener(this);
        findViewById(R.id.iv_help_call_3).setOnClickListener(this);
        findViewById(R.id.iv_help_call_4).setOnClickListener(this);
        helpAnswer();
    }

    private void helpAnswer() {
        App.getInstance().getStorage().getTrueCase().observe((LifecycleOwner) mContext, new Observer<Integer>() {
            @Override
            public void onChanged(Integer trueCase) {
                Log.i(HelpCallDialog.TAG, "Đáp án: " + trueCase);
                switch (trueCase) {
                    case 1:
                        mAnswer = "Tôi nghĩ là A em ạ!";
                        break;

                    case 2:
                        mAnswer = "Chắc là B đó em!";
                        break;

                    case 3:
                        mAnswer = "Tôi nghĩ là C em ạ!";
                        break;

                    case 4:
                        mAnswer = "Khả năng cao là D";
                        break;

                    default:
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_help_call_1) {
            dismiss();
            showAnswerDialog(R.drawable.ic_neos_thanh, "Neos.Thanh", mAnswer);
        } else if (v.getId() == R.id.iv_help_call_2) {
            dismiss();
            showAnswerDialog(R.drawable.ic_toi_di_code_dao, "Tôi đi code dạo", mAnswer);
        } else if (v.getId() == R.id.iv_help_call_3) {
            dismiss();
            showAnswerDialog(R.drawable.ic_bill_gate, "Bill Gates", mAnswer);
        } else if (v.getId() == R.id.iv_help_call_4) {
            dismiss();
            showAnswerDialog(R.drawable.ic_mark_zuck, "Mark", mAnswer);
        }
    }

    private void showAnswerDialog(int avatar, String nickName, String answer) {
        MediaManager.getInstance().playGame(R.raw.song_call, new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                HelpCallAnswerDialog dialog = new HelpCallAnswerDialog(mContext, new HelpCallAnswerDialog.OnHelpCallAnswer() {
                    @Override
                    public void callBack() {
                        callBack.callBack();
                    }
                });
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                dialog.updateInfo(avatar, nickName, answer);
                dialog.show();
            }
        });
    }

    public interface OnHelpCall {
        void callBack();
    }
}
