package com.techja.ailatrieuphu.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.techja.ailatrieuphu.App;
import com.techja.ailatrieuphu.R;

public class CurrentQuestionDialog extends Dialog {
    public static final String TAG = CurrentQuestionDialog.class.getName();
    private final Context mContext;
    private TextView tv2Hundred, tv4Hundred, tv6Hundred, tv1Million, tv2Million, tv3Million,
            tv6Million, tv10Million, tv14Million, tv22Million, tv30Million, tv40Million,
            tv60Million, tv85Million, tv150Million;

    public CurrentQuestionDialog(@NonNull Context context) {
        super(context);
        mContext = context;
        setContentView(R.layout.view_current_question);
        initViews();
    }

    private void initViews() {
        tv2Hundred = findViewById(R.id.tv_2_hundred);
        tv4Hundred = findViewById(R.id.tv_4_hundred);
        tv6Hundred = findViewById(R.id.tv_6_hundred);
        tv1Million = findViewById(R.id.tv_1_million);
        tv2Million = findViewById(R.id.tv_2_million);
        tv3Million = findViewById(R.id.tv_3_million);
        tv6Million = findViewById(R.id.tv_6_million);
        tv10Million = findViewById(R.id.tv_10_million);
        tv14Million = findViewById(R.id.tv_14_million);
        tv22Million = findViewById(R.id.tv_22_million);
        tv30Million = findViewById(R.id.tv_30_million);
        tv40Million = findViewById(R.id.tv_40_million);
        tv60Million = findViewById(R.id.tv_60_million);
        tv85Million = findViewById(R.id.tv_85_million);
        tv150Million = findViewById(R.id.tv_150_million);

        stepQuestion();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        }, 2000);
    }


    private void stepQuestion() {
        App.getInstance().getStorage().getMoneyLevel().observe((LifecycleOwner) mContext, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                receiveMoneyLevel(integer);
            }
        });
    }

    private void receiveMoneyLevel(Integer integer) {
        switch (integer) {
            case 2:
                tv2Hundred.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 3:
                tv2Hundred.setBackgroundResource(0);
                tv4Hundred.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 4:
                tv4Hundred.setBackgroundResource(0);
                tv6Hundred.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 5:
                tv6Hundred.setBackgroundResource(0);
                tv1Million.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 6:
                tv1Million.setBackgroundResource(0);
                tv2Million.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 7:
                tv2Million.setBackgroundResource(0);
                tv3Million.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 8:
                tv3Million.setBackgroundResource(0);
                tv6Million.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 9:
                tv6Million.setBackgroundResource(0);
                tv10Million.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 10:
                tv10Million.setBackgroundResource(0);
                tv14Million.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 11:
                tv14Million.setBackgroundResource(0);
                tv22Million.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 12:
                tv22Million.setBackgroundResource(0);
                tv30Million.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 13:
                tv30Million.setBackgroundResource(0);
                tv40Million.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 14:
                tv40Million.setBackgroundResource(0);
                tv60Million.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 15:
                tv60Million.setBackgroundResource(0);
                tv85Million.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            case 16:
                tv85Million.setBackgroundResource(0);
                tv150Million.setBackgroundResource(R.drawable.bg_money_curent);
                break;

            default:
                break;
        }
    }
}
