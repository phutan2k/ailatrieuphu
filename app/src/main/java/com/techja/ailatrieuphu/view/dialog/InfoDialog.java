package com.techja.ailatrieuphu.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.techja.ailatrieuphu.R;

public class InfoDialog extends Dialog implements View.OnClickListener {
    private final OnInfoCallBack callBack;
    private final Context mContext;

    public InfoDialog(@NonNull Context context, OnInfoCallBack callBack) {
        super(context);
        mContext = context;
        this.callBack = callBack;
        setContentView(R.layout.view_intro);
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        initViews();
    }

    private void initViews() {
        findViewById(R.id.bt_back_info).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_back_info) {
            dismiss();
        }
    }

    public interface OnInfoCallBack {
        void callBack();
    }
}
